import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,
  ImageBackground,
} from "react-native";

export default function GetStarted({ navigation }) {
  
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../../../assets/BackgroundGetStart.png')}
        style={styles.image}
      >
        <Text style={{ fontSize: 20, fontWeight: "bold" }}></Text>
        {/* <Image
        style={{ height: 150, width: 150 }}
        source={require("../assets/logo.jpg")}
      /> */}
        <View
          style={{
            backgroundColor: "white",
            height: "30%",
            borderRadius: 25,
            marginTop: "130%",
            paddingTop: 30,
          }}
        >
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate("Login")}
          >
            <Text style={{ color: "white", fontSize: 18 }}>Login</Text>
          </TouchableOpacity>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}></Text>
          <TouchableOpacity
            style={styles.button2}
            onPress={() => navigation.navigate("Register")}
          >
            <Text style={{ color: "white", fontSize: 18 }}>Register</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#A0E7E5",
  },
  button: {
    alignItems: "center",
    backgroundColor: "#81B622",
    padding: 10,
    width: "90%",
    marginLeft: "5%",
    borderRadius: 20,
  },
  button2: {
    alignItems: "center",
    backgroundColor: "#7EC8E3",
    padding: 10,
    width: "90%",
    marginLeft: "5%",
    borderRadius: 20,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
});
