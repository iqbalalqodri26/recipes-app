import React, {useState,useEffect} from 'react'
import { StyleSheet, Text, View,TextInput,Button,ImageBackground,TouchableOpacity } from 'react-native'
import { Card, ListItem, CheckBox, Icon } from "react-native-elements";

import * as firebase  from 'firebase'
export default function Register({navigation}) {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

   const firebaseConfig = {
    apiKey: "AIzaSyBXJ1ph_qwdQxXmY6CW2bZNhq2RNf6ivPs",
    authDomain: "finalprojectreactnativeexpo.firebaseapp.com",
    projectId: "finalprojectreactnativeexpo",
    storageBucket: "finalprojectreactnativeexpo.appspot.com",
    messagingSenderId: "475743313831",
    appId: "1:475743313831:web:8dadc9b4d27eaaf5dae082"
  };

    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    } 

    const submit =()=>{
        const data = {
            email,password
        }

        console.log(data);
        firebase.auth().createUserWithEmailAndPassword(email,password)
        .then(()=>{
             alert('Selamat Anda Berhasil Register')
            console.log('berhasil register');
            navigation.navigate('Home')
        }).catch(()=>{
             alert(' Anda Gagal Register')
            console.log('gagal register');

        })

    } 
  
    return (
        <View style={styles.container}>
        <ImageBackground
        source={require("../../../assets/Background.png")}

        style={styles.image}
      >
        <Card> 
          <Text style={{fontSize:30,marginLeft:'35%',marginTop: 10,marginBottom: 25,}} >Register</Text>
            <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Username"
          value = {email}
          onChangeText = {(value)=>setEmail(value)}

        />
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Password"
          value = {password}
          onChangeText = {(value)=>setPassword(value)}
        />

        <TouchableOpacity style={styles.button2} onPress={submit}>
              <Text style={{ color: "white", fontSize: 18 }}>Submit</Text>
            </TouchableOpacity>
            <Text style={{ fontSize: 20, fontWeight: "bold" }}></Text>
            <TouchableOpacity
              style={styles.button}
              onPress={()=>navigation.navigate('Login')}
            >
              <Text style={{ color: "white", fontSize: 18 }}>Sudah Punya Akun ?</Text>
            </TouchableOpacity>
        </Card>
        </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center'
    },
    image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  button: {
    alignItems: "center",
    backgroundColor: "#81B622",
    padding: 10,
    width: "90%",
    marginLeft: "5%",
    borderRadius: 20,
  },
  button2: {
    alignItems: "center",
    backgroundColor: "#7EC8E3",
    padding: 10,
    width: "90%",
    marginLeft: "5%",
    borderRadius: 20,
  },
})
