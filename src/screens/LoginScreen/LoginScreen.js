import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TextInput,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import * as firebase from "firebase";
import { Card, ListItem, CheckBox, Icon } from "react-native-elements";

export default function Login({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const firebaseConfig = {
    apiKey: "AIzaSyBXJ1ph_qwdQxXmY6CW2bZNhq2RNf6ivPs",
    authDomain: "finalprojectreactnativeexpo.firebaseapp.com",
    projectId: "finalprojectreactnativeexpo",
    storageBucket: "finalprojectreactnativeexpo.appspot.com",
    messagingSenderId: "475743313831",
    appId: "1:475743313831:web:8dadc9b4d27eaaf5dae082",
  };
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  const submit = () => {
    console.log("ok");
    const data = {
      email,
      password,
    };

    console.log(data);
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        console.log("berhasil Login");
        alert("Selamat Anda Berhasil Login");
        navigation.navigate("Home");
      })
      .catch(() => {
        alert("Anda Gagal Login");
        console.log("gagal Login");
      });
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../../assets/Background.png")}
        style={styles.image}
      >
        <Text style={{ fontSize: 20, fontWeight: "bold" }}></Text>

        <Card >
          <Text style={{fontSize:30,marginLeft:'40%',marginTop: 10,marginBottom: 25,}} >Login</Text>
          <View>
            <TextInput
              style={{
                borderWidth: 1,
                paddingVertical: 10,
                borderRadius: 5,
                width: 300,
                marginBottom: 10,
                paddingHorizontal: 10,
              }}
              placeholder="Masukan Username"
              value={email}
              value={email}
              onChangeText={(value) => setEmail(value)}
            />
            <TextInput
              style={{
                borderWidth: 1,
                paddingVertical: 10,
                borderRadius: 5,
                width: 300,
                marginBottom: 30,
                paddingHorizontal: 10,
              }}
              placeholder="Masukan Password"
              value={password}
              onChangeText={(value) => setPassword(value)}
            />

            <TouchableOpacity style={styles.button} onPress={submit}>
              <Text style={{ color: "white", fontSize: 18 }}>Login</Text>
            </TouchableOpacity>
            <Text style={{ fontSize: 20, fontWeight: "bold" }}></Text>
            <TouchableOpacity
              style={styles.button2}
              onPress={() => navigation.navigate("Register")}
            >
              <Text style={{ color: "white", fontSize: 18 }}>Buat Akun</Text>
            </TouchableOpacity>
          </View>
        </Card>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  button: {
    alignItems: "center",
    backgroundColor: "#81B622",
    padding: 10,
    width: "90%",
    marginLeft: "5%",
    borderRadius: 20,
  },
  button2: {
    alignItems: "center",
    backgroundColor: "#7EC8E3",
    padding: 10,
    width: "90%",
    marginLeft: "5%",
    borderRadius: 20,
  },
});
